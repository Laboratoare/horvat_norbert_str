package s7sem1;

import java.util.concurrent.Semaphore;

public class Main {


    public static void main(String[] args) {
        final Semaphore P9 = new Semaphore(1, true);

        Locatie P0 = new Locatie();
        P0.activity(0);
        Tranzitie T0 = new Tranzitie();
        T0.activity(0);


        Thread1 thread1 = new Thread1(P9);
        thread1.start();

        Thread2 thread2 = new Thread2(P9);
        thread2.start();

    }


}
