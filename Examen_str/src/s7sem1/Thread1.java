package s7sem1;

import java.util.concurrent.Semaphore;

public class Thread1 extends Thread {
    Semaphore P9;

    Thread1(Semaphore P9) {

        this.P9 = P9;

    }

    public void run() {

        Locatie P2 = new Locatie();
        P2.activity(2);

        Tranzitie T1 = new Tranzitie();
        T1.activity(1,3,1);

        Locatie P4 = new Locatie();
        P4.activity(4);


        try {
            P9.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Tranzitie T4 = new Tranzitie();
        T4.activity(4);

        Locatie P6 = new Locatie();

        P6.activity(4,6,6);
        P9.release();

        Tranzitie T7 = new Tranzitie();
        T7.activity(7);

        Locatie P9 = new Locatie();
        P9.activity(9);

        Tranzitie T9 = new Tranzitie();
        T9.activity(9);







    }


}
