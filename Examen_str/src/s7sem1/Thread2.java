package s7sem1;

import java.util.concurrent.Semaphore;

public class Thread2 extends Thread {
    Semaphore P9;

    Thread2(Semaphore P9) {

        this.P9 = P9;

    }

    public void run() {

        Locatie P3 = new Locatie();
        P3.activity(3);

        Tranzitie T2 = new Tranzitie();
        T2.activity(2,4,2);

        Locatie P5 = new Locatie();
        P5.activity(5);


        try {
            P9.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Tranzitie T5 = new Tranzitie();
        T5.activity(5);

        Locatie P7 = new Locatie();

        P7.activity(3,5,7);
        P9.release();

        Tranzitie T8 = new Tranzitie();
        T8.activity(8);

        Locatie P10 = new Locatie();
        P10.activity(10);

        Tranzitie T10 = new Tranzitie();
        T10.activity(10);







    }


}
