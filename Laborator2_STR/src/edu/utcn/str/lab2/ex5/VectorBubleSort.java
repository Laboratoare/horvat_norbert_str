package edu.utcn.str.lab2.ex5;

import java.util.Scanner;


public class VectorBubleSort {
    public static void main(String[] args) {

        int i, ok = 0, aux, n;
        int v[];
        v = new int[6];
        System.out.println("Numarul de elemente din vector: ");
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        for (i = 0; i <= n - 1; i++) {

            v[i] = s.nextInt();
            System.out.println("Element din vectorul V["+i+"]"+" este "+ v[i]);
        }
        while (ok == 0) {
            ok = 1;
            for (i = 0; i <= n - 2; i++) {
                if (v[i] > v[i + 1]) {
                    aux = v[i];
                    v[i] = v[i + 1];
                    v[i + 1] = aux;
                    ok = 0;
                }
            }
        }
        for (i = 0; i < n; i++) {
            System.out.print(v[i] + " ");
        }
    }
}




