package edu.utcn.str.lab2.ex6;

import  java.util.Scanner;


public class FactorialNerecursivRecursiv {

    static void nonrecursive() {
        int i, fact = 1, n;

        System.out.println("Numarul factorial nerecursiv ");
        Scanner s = new Scanner(System.in);
        n = s.nextInt();

        for (i = 1; i <= n; i++) {
            fact = fact * i;
        }

        System.out.println("Factorialul numarului " + n + " este: " + fact+" cu functie nerecursiva");

    }




    static int factorialrecursiv(int n){
        if (n == 0)
            return 1;
        else
            return(n * factorialrecursiv(n-1));
    }





    public static void main(String[] args) {
        //Functie nonrecursiva pentru numere factoriale
         nonrecursive();

        int i,fact=1,n;
        System.out.println("Numarul factorial recursiv ");
        Scanner s = new Scanner(System.in);
        n = s.nextInt();
        fact = factorialrecursiv(n);
        System.out.println("Factorialul numarului "+n+" este: "+fact+" cu functie recursiva");


    }


}
