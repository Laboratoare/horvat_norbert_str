package edu.utcn.str.lab3.ex1;

import javax.xml.namespace.QName;

public class Robot {

    int x;
    String name;

    Robot() {
        x = -1;
        name = "Robot_0";
    }

    void change(int k) {
        if (k >= 1)
            x += k;

    }

    @Override
    public String toString() {
        return "Robot{" + "x=" + x + ", name=" + name + '}';
    }
}
