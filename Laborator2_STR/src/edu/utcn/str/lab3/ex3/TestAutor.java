package edu.utcn.str.lab3.ex3;

import java.util.Scanner;

public class TestAutor {
    public static void main(String[] args) {

        String a, b;
        char c;
        Scanner s = new Scanner(System.in);
        System.out.println("Nume autor: ");
        a = s.next();
        System.out.println("Adresa email: ");
        b = s.next();
        System.out.println("Sex: ");
        c = s.next().charAt(0);

        Autor aut1 = new Autor(a, b, c);
        System.out.println(aut1);
    }
}
