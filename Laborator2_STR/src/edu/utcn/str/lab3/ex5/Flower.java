package edu.utcn.str.lab3.ex5;


public class Flower {


    public static int numberofobjects = 0;

    int petal;
    static int dieCount = 0;

    Flower(int p) {
        petal = p;
        System.out.println("New flower has been created!");
        numberofobjects++;
        System.out.println("There are " + Flower.numberofobjects + " objects in this class");
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        Flower f3 = new Flower(3);

        numberofobjects++;
    }
}