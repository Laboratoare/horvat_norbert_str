package str.curs2.ex1thread;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        // create the thread objects
        MyThread myThread1 = new MyThread("Ioan");
        MyThread myThread2 = new MyThread("Gheorghe");

        // start the thread objects
        myThread1.start();
        myThread2.start();
    }
}
