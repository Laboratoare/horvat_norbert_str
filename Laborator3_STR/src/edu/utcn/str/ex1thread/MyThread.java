package str.curs2.ex1thread;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    public static final int MSG_NUM = 5;

    public MyThread(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < MSG_NUM; i++) {
            System.out.println(String.format("%s: Message number %d", this.getName(), i));

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
        }
    }
}
