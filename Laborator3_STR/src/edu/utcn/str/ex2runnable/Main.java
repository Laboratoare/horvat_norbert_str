package str.curs2.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Thread.currentThread().setName("Principal");

        // create the runnable objects
        MyThread myThread1 = new MyThread("Ioan");
        MyThread myThread2 = new MyThread("Gheorghe");

        // start the runnable objects by the use of Thread
//        new Thread(myThread1).start();
//        new Thread(myThread2).start();

        // refactor the starting method a little bit
        myThread1.start();
        myThread2.start();
        MyThreadUtils.printMessages(Thread.currentThread().getName());

        // in this case we will only have one thread
//        myThread1.run();
//        myThread2.run();
    }
}
