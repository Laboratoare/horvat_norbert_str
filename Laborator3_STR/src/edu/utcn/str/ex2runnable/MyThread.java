package str.curs2.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread implements Runnable {
    private String name;

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        MyThreadUtils.printMessages(Thread.currentThread().getName());
    }

    public void start() {
        Thread thread = new Thread(this);
        thread.setName(name);
        thread.start();
    }
}
