package str.curs2.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThreadUtils {
    public static final int MSG_NUM = 5;

    public static void printMessages(String name) {
        for (int i = 0; i < MSG_NUM; i++) {
            System.out.println(String.format("%s: Message number %d", name, i));

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
            }
        }
    }
}
