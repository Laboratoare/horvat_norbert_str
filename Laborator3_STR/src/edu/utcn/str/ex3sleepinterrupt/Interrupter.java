package str.curs2.ex3sleepinterrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Interrupter extends Thread {
    private Thread interruptedThread;

    public Interrupter(Thread interruptedThread) {
        this.interruptedThread = interruptedThread;
    }

    @Override
    public void run() {
        this.setName("Interrupter");

        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
        }

        System.out.println(this.getName() + ": Now I will interrupt the slow thread");

        this.interruptedThread.interrupt();
    }
}
