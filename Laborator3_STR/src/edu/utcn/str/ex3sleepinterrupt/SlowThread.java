package str.curs2.ex3sleepinterrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class SlowThread extends Thread {
    @Override
    public void run() {
        this.setName("Slow");
        System.out.println(String.format("%s: do something then wait 24h", this.getName()));

        try {
            Thread.sleep(24 * 60 * 60 * 1000);
        } catch (InterruptedException e) {
            System.err.println(this.getName() + ": My sleep was interrupted");
        }
    }
}
