package str.curs2.ex4priority;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 8; i++) {
            new MyThread("Thread with priority " + 3, 3).start();
        }

        for (int i = 0; i < 8; i++) {
            new MyThread("Thread with priority " + 9, 9).start();
        }

        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        MyThread.setStop(true);

        try {
            Thread.sleep(3 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
