package str.curs2.ex4priority;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class MyThread extends Thread {

    private static boolean stop;
    private int counter;

    public MyThread(String name, int priority) {
        this.setName(name);
        this.setPriority(priority);
    }

    @Override
    public void run() {
        while (!stop) {
            for (long i = 0; i < 100000000l; i++) {
                i++;
                i--;
            }

            counter++;
        }

        System.out.println(String.format("%s: %d", this.getName(), counter));
    }

    public static void setStop(boolean stop) {
        MyThread.stop = stop;
    }
}
