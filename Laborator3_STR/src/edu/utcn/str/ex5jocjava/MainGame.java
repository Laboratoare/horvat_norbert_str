package edu.utcn.str.ex5jocjava;



import edu.utcn.str.ex5jocjava.engine.RockThread;
import edu.utcn.str.ex5jocjava.gui.MainCharacter;
import edu.utcn.str.ex5jocjava.gui.Rock;
import edu.utcn.str.ex5jocjava.gui.Score;
import edu.utcn.str.ex5jocjava.gui.Window;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainGame extends JFrame implements Runnable {

    private static Rock rock2;
    public Boolean stop = false;

    public void run() {

        while (!stop) {
            MainCharacter mainCharacter = new MainCharacter();
            Score score = new Score();
            Window w = new Window(mainCharacter, score);
            String filepathto = ("src/edu/utcn/str/ex5jocjava/bgmusic.wav");
            playaudio(filepathto);
            float speed = 1f;
            while (!stop) {
                try {
                    w.addRocks(generateRocks(speed, mainCharacter));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (speed >= 0.9)
                    speed -= 0.1;
                else if (speed == 0.9) {
                    speed = (float) 0.9;
                }

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }

            }

        }
    }

    private static List<Rock> generateRocks(float speed, MainCharacter mainCharacter) throws InterruptedException {
        List<Rock> rocks = new ArrayList<>();
        ExecutorService executor = Executors.newSingleThreadExecutor();

        for (int i = 0; i < 10; i++) {
            rock2 = new Rock((int) (Math.random() * Window.windowSize), 60);  //positioning of random rocks
        }
        Runnable runner = new RockThread(rock2, speed, mainCharacter);
        executor.submit(runner);
        rocks.add(rock2);
        return rocks;

    }


    private static void playaudio(String filepath) {
        try {
            File musicloc = new File(filepath);
            if (musicloc.exists()) {
                AudioInputStream audioinput = AudioSystem.getAudioInputStream(musicloc);
                Clip clip = AudioSystem.getClip();
                clip.open(audioinput);
                FloatControl gainControl =
                        (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                gainControl.setValue(-27.0f); // Reduce volume by 10 decibels.
                clip.loop(Clip.LOOP_CONTINUOUSLY);
                clip.start();
            } else {
                System.out.println("cant find ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }


    public Boolean getStop() {
        return stop;
    }

    public void setStop(Boolean stop) {
        this.stop = stop;
    }

}



