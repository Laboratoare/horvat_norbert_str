package edu.utcn.str.ex5jocjava.engine;



import edu.utcn.str.ex5jocjava.gui.MainCharacter;
import edu.utcn.str.ex5jocjava.gui.Rock;
import edu.utcn.str.ex5jocjava.gui.Window;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.awt.*;
import java.io.File;

public class RockThread extends Thread implements Runnable {
    private boolean colided = false;
    private boolean colcheck = false;
    private Rock rocks;
    private MainCharacter mainCharacter;
    private float speed;
    private int health = 100;


    private static void playaudio(String filepath) {
        try {
            File musicloc = new File(filepath);
            if (musicloc.exists()) {
                AudioInputStream audioinput = AudioSystem.getAudioInputStream(musicloc);
                Clip clip = AudioSystem.getClip();
                clip.open(audioinput);
                FloatControl gainControl =
                        (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                gainControl.setValue(-25.0f); // Reduce volume by 10 decibels.
                clip.start();
            } else {
                System.out.println("cant find ");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public RockThread(Rock rock, float speed, MainCharacter mainCharacter) {
        this.rocks = rock;
        this.speed = speed;
        this.mainCharacter = mainCharacter;

    }


    public void run() {


        while ((rocks.getY() < Window.windowSize)) { // TODO: or is collision
            ;
            colided = false;
            try {
                Thread.sleep((long) (5 * this.speed));
                checkcolision();


                //validate once every colision
                if ((colided) && (!colcheck)) {
                    System.out.println(" Player Collided ");
                    colcheck = true;
                    String filepathto = ("src/edu/utcn/str/JocJava/engine/trump.wav");
                    playaudio(filepathto);
                    health -= 10;
                }

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            rocks.setLocation(rocks.getX(), rocks.getY() + 1);
            rocks.repaint();

        }


    }

    private void checkcolision() {
        Rectangle r4 = mainCharacter.getBounds();
        Rectangle r2 = rocks.getBounds();

        if ((r4.contains(r2)) && (r4.intersects(r2))) {
            colided = true;
        }
    }


}
