package edu.utcn.str.ex5jocjava.gui;

import javax.swing.*;
import java.awt.*;


public class MainCharacter extends JComponent {

    static int chrWidth = 60;
    static int chrHeight = 40;
    private Image image2;

    {
        image2 = Toolkit.getDefaultToolkit().createImage("src/edu/utcn/str/ex5jocjava/gui/res/trump.gif");
    }

    @Override
    public void paint(Graphics g) {
        this.setSize(180, 190);

        if (image2 != null) {

            g.drawImage(image2, chrHeight, chrWidth, Color.WHITE, this);
        }

    }
}
