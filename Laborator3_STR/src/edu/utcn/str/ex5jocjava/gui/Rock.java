package edu.utcn.str.ex5jocjava.gui;

import javax.swing.*;
import java.awt.*;

public class Rock extends JComponent {

    private Image fire;

    {
        fire = Toolkit.getDefaultToolkit().createImage("src/edu/utcn/str/ex5jocjava/gui/res/fire.gif");
    }

    public Rock(int x, int y) {
        this.setLocation(x, y);
    }

    @Override
    public void paint(Graphics g) {

        // g.setColor(Color.BLACK);
        // g.fillRect(0, 0, 20, 20);
        this.setAlignmentX(CENTER_ALIGNMENT);
        this.setSize(80, 70);
        this.setBackground(Color.black);
        if (fire != null) {
            int rockSize = 0;
            g.drawImage(fire, rockSize, rockSize, Color.WHITE, this);
        }
    }
}


