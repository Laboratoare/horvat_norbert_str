package edu.utcn.str.ex5jocjava.gui;



import edu.utcn.str.ex5jocjava.MainGame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class StartPage extends JFrame {
    boolean startcondition = false;
    public int i = 0;

    public StartPage() {
        JFrame frame = new JFrame();

        setBounds(100, 20, 250, 140);
        JPanel panel = new JPanel();

        JLabel Name = new JLabel("Dodge the Fire Trump ");
        JButton button_start = new JButton("Start");
        JButton button_stop = new JButton("Stop");
        Name.setFont(new Font("TimesRoman", Font.BOLD, 20));

        Name.setLocation(0, 0);
        button_start.setBounds(0, 0, 100, 100);
        button_stop.setBounds(0, 0, 100, 200);
        panel.add(Name);
        panel.add(button_start);
        panel.add(button_stop);
        panel.setBounds(0, 0, 240, 200);
        button_start.addActionListener(this::actionPerformed);
        button_stop.addActionListener(this::actionPerformed);
        add(panel);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        this.setVisible(true);
    }


    public void actionPerformed(ActionEvent e) {

        // Cover cover =new Cover();
/*
        Thread th=new Thread(cover);*/
        ExecutorService executorstart = Executors.newSingleThreadExecutor();
        //Runnable cover = new Cover();
        Future<?> cover = executorstart.submit(new MainGame());

        switch (e.getActionCommand()) {

            case "Start":

                startcondition = true;
                if (i == 0) {
                    System.out.println("start");
                    i = 1;
                    executorstart.submit((Runnable) cover);
                    // th.start();

                } else {
                    System.out.println("Game allready started");

                }

                break;
            case "Stop":
                System.out.println("stop");
                System.out.println("under development");
                cover.cancel(true);

                //  cover.setStop(true);

                break;


            default:
                break;
        }
    }


}

