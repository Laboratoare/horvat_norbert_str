package edu.utcn.str.ex5jocjava.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;


public class Window extends JFrame {

    static public int windowSize = 1000;
    private int pmovespeed = 8;
    private int mlimitstanga = -40;
    private int mlimitdreapta = 867;


    public Window(MainCharacter mainCharacter, Score score) {


        {
            setBounds(500, 20, windowSize, windowSize);
            //    setBounds(-1100, 20, windowSize, windowSize);
            setLayout(null);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setResizable(false);
            this.getContentPane().setBackground(Color.white);
            //    this.setBackground(Color.black);
            score.setBounds(windowSize / 2 - 60, (20), MainCharacter.chrWidth, MainCharacter.chrHeight);

            mainCharacter.setBounds(windowSize / 2, (int) (windowSize / 1.238), MainCharacter.chrWidth, MainCharacter.chrHeight);
        }


        add(mainCharacter);
        add(score);


        this.addKeyListener(new KeyListener() {


            @Override
            public void keyPressed(KeyEvent e) {
                if ((e.getKeyCode() == 37) || (e.getKeyChar() == ('a'))) {
                    mainCharacter.setLocation(mainCharacter.getX() - pmovespeed, mainCharacter.getY());
                } else if ((e.getKeyCode() == 39) || (e.getKeyChar() == ('d'))) {
                    mainCharacter.setLocation(mainCharacter.getX() + pmovespeed, mainCharacter.getY());

                }

                if (mainCharacter.getX() <= mlimitstanga) {
                    mainCharacter.setLocation(mlimitstanga, mainCharacter.getY());
                } else if (mainCharacter.getX() > mlimitdreapta) {
                    mainCharacter.setLocation(mlimitdreapta, mainCharacter.getY());
                }
                // System.out.println(mainCharacter.getX());


            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

        });

        this.setVisible(true);
    }

    public void addRocks(List<Rock> rocks) {
        for (Rock rock : rocks) {
            rock.setBounds(rock.getX(), rock.getY(), windowSize / 6, windowSize / 6);             //set dimenion of a rock
            add(rock);
        }
    }

}
