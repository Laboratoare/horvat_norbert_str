package edu.utcn.str.aplicatia1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainAp1 {
    public static void main(String[] args) {

        Lock P9 = new ReentrantLock();
        Lock P10 = new ReentrantLock();

        Locatie P0 = new Locatie();
        P0.activity(0);

        Tranzitie T1 = new Tranzitie();
        T1.Tran(1);

        Thread1 thread1 = new Thread1(P9);
        thread1.start();

        Thread2 thread2 = new Thread2(P9, P10);
        thread2.start();

        Thread3 thread3 = new Thread3(P10);
        thread3.start();

    }
}



