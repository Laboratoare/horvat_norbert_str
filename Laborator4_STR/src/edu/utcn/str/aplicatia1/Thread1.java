package edu.utcn.str.aplicatia1;

import java.util.concurrent.locks.Lock;

public class Thread1 extends Thread {
    Lock P9;

    Thread1(Lock P9) {

        this.P9 = P9;
    }

    public void run() {
        Locatie P1 = new Locatie();
        P1.activity(1);

        Tranzitie T2 = new Tranzitie();

        P9.lock();

        T2.Tran(2);

        Locatie P4 = new Locatie();
        P4.activity(2, 4, 4);

        Tranzitie T4 = new Tranzitie();
        T4.Tran(4, 4);

        P9.unlock();

        Locatie P6 = new Locatie();
        P6.activity(6);

        Tranzitie T6 = new Tranzitie();
        T6.Tran(6);

    }
}
