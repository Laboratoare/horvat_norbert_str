package edu.utcn.str.aplicatia1;

public class Tranzitie {

    /*

     * intT - timpul tranzitiei
     * NrTranzitie - numarul tranzitiei in care se afla sistemul
     */
    public void Tran(int intT, int NrTranzitie) {
        System.out.println("Tranzitia T" + NrTranzitie);
        try {
            Thread.currentThread().sleep(intT * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*

     * intNrTranzitie - numarul tranzitiei
     */

    public void Tran(int NrTranzitie) {
        System.out.println("Tranzitia T" + NrTranzitie);
    }
}

