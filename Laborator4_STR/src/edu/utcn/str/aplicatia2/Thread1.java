package edu.utcn.str.aplicatia2;

import java.util.concurrent.locks.Lock;

public class Thread1 extends Thread
{

    Lock P9, P10;
    Thread1(Lock P9, Lock P10)
    {
        this.P9 = P9;
        this.P10 = P10;
    }

    public void run()
    {
        Locatie P1= new Locatie();
        P1.activity(2, 4, 1);
        Tranzitie T2 = new Tranzitie();
        P9.lock();
        T2.activity(2);
        Locatie P4 = new Locatie();
        P10.lock();
        P4.activity(4, 6, 4);
        Tranzitie T4 = new Tranzitie();
        T4.activity(4);
        Locatie P6 = new Locatie();
        P6.activity(6);
        Tranzitie T6 = new Tranzitie();
        T6.activity(4, 6);
        P9.unlock();
        P10.unlock();
        Locatie P7 = new Locatie();
        P7.activity(7);
        Tranzitie T8 = new Tranzitie();
        T8.activity(4, 8);

        System.out.println("S-a terminat thread 1");
    }
}
