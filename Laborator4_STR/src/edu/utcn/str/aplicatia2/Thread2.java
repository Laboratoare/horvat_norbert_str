package edu.utcn.str.aplicatia2;


import java.util.concurrent.locks.Lock;

public class Thread2 extends Thread
{
    Lock P9;
    Lock P10;
    Thread2(Lock P9, Lock P10)
    {

        this.P9 = P9;
        this.P10 = P10;
    }

    public void run()
    {
        Locatie P2 = new Locatie();
        P2.activity(3, 5, 2);
        Tranzitie T3 = new Tranzitie();
        T3.activity(3);
        Locatie P3 = new Locatie();
        P10.lock();
        P3.activity(5, 7, 3);
        Tranzitie T5 = new Tranzitie();
        P9.lock();
        T5.activity(5);
        Locatie P5 = new Locatie();
        P5.activity(5);
        Tranzitie T7 = new Tranzitie();
        T7.activity(5, 7);
        P10.unlock();
        P9.unlock();
        Locatie P8= new Locatie();
        P8.activity(8);
        Tranzitie T9 = new Tranzitie();
        T9.activity(5, 9);

        System.out.println("S-a terminat thread 2");
    }
}
