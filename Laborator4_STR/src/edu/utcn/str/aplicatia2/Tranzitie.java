package edu.utcn.str.aplicatia2;

public class Tranzitie
{


    /*

     * intT - timpul tranzitiei
     * NrTranzitie - numarul tranzitiei in care se afla sistemul
     */

    public void activity(int intT, int NrTranzitie)
    {
        System.out.println("Tranzitia T" +NrTranzitie +" timpul tranzitiei:"+intT);
        try
        {
            Thread.currentThread().sleep(intT*1000);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*

     * NrTranzitie - numarul tranzitiei
     */

    public void activity(int NrTranzitie)
    {

        System.out.println("Tranzitia T"+NrTranzitie);
    }
}
