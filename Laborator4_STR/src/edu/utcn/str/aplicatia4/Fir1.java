package edu.utcn.str.aplicatia4;
public class Fir1 extends Thread {

	Object P6, P10;
	int sleep_min, sleep_max, wait;

	public Fir1( Object P6,Object P10,int sleep_min, int sleep_max, int wait) {

		this.P6 =P6;
		this.P10 =P10;
		this.sleep_min = sleep_min;
		this.sleep_max = sleep_max;
		this.wait = wait;

	}

	public void run() {
		System.out.println(this.getName() +" Locatia : "+ "P0");
		try {
			Thread.sleep(wait * 1000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}

		System.out.println(this.getName() +" Locatia : "+ "P1" + " sleep_min:"+sleep_min +" sleepmax:"+sleep_max);

		synchronized(P6)
		{
			P6.notify();}
		synchronized(P10)
		{
			P10.notify();}
		
		int k = (int) Math.round(Math.random() * (sleep_max - sleep_min) + sleep_min);
		for (int i = 0; i < k * 100000; i++) {
			i++;
			i--;
		}

		
		System.out.println(this.getName() +" Locatia : "+ "P2");
	}
}
