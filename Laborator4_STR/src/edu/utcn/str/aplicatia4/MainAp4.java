package edu.utcn.str.aplicatia4;

public class MainAp4 {

	public static void main(String[] args)  {
		Object P6 = 1;
		Object P10 = 2;

		Fir1 fir1 = new Fir1(P6, P10, 2, 3, 7);
		Fir2 fir2 = new Fir2(P6, 3, 5);
		Fir3 fir3 = new Fir3(P10, 4, 6);
		fir1.start();
		fir2.start();
		fir3.start();

		try {
			fir1.join();
			fir2.join();
			fir3.join();
		}

		catch (InterruptedException ignored) {


		}
	}
}
