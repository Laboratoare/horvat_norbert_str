package edu.utcn.str.aplicatia1_semaphore_CDL;

public class Locatie
{

    /*

     * intTmin - timpul minim
     * intTmax - timpul maxim
     * intNrLocatie - locatia in care se afla jetonul
     */

    public void activity(int intTmin, int intTmax, int NrLocatie)
    {
        int k = (int)(Math.random()*(intTmax-intTmin)+intTmin);
        int j=0;
        System.out.println("Locatie P"+NrLocatie+" timpul minim: "+intTmin+" timpul maxim: "+intTmax);
        for(int i=0; i<k*1000000; i++)
        {
            j++;
            j--;
        }
    }

    /*

     * intNrLocatia - locatia in care se afla jetonul
     */

    public void activity(int NrLocatie)
    {
        System.out.println("Locatie P"+NrLocatie);
    }


}
