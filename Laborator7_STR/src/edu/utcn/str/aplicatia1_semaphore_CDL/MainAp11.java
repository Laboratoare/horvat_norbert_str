package edu.utcn.str.aplicatia1_semaphore_CDL;



import java.util.concurrent.CountDownLatch;

import java.util.concurrent.Semaphore;



public class MainAp11 {


    public static void main(String[] args) {


        final Semaphore P9 = new Semaphore(1, true);
        final Semaphore P10 = new Semaphore(1, true);

        CountDownLatch countDownLatch = new CountDownLatch(2);

        Thread1 thread1 = new Thread1(P9, P10,countDownLatch );
        thread1.start();

        Thread2 thread2 = new Thread2(P9, P10,countDownLatch);
        thread2.start();


    }

}












