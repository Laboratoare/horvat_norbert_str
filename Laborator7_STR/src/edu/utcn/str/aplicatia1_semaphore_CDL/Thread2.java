package edu.utcn.str.aplicatia1_semaphore_CDL;


import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;


public class Thread2 extends Thread
{
    CountDownLatch count;
    Semaphore  P9;
    Semaphore P10;
    Thread2(Semaphore  P9, Semaphore  P10,CountDownLatch count)
    {
        this.count = count;
        this.P9 = P9;
        this.P10 = P10;
    }

    public void run() {
        while (true) {
            Locatie P2 = new Locatie();
            P2.activity(3, 5, 2);
            Tranzitie T3 = new Tranzitie();
            T3.activity(3);
            Locatie P3 = new Locatie();
            try {
                P10.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            P3.activity(5, 7, 3);
            Tranzitie T5 = new Tranzitie();

            try {
                P9.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            T5.activity(5);
            Locatie P5 = new Locatie();
            P5.activity(5);
            Tranzitie T7 = new Tranzitie();
            T7.activity(5, 7);
            P10.release();
            P9.release();
            Locatie P8 = new Locatie();
            P8.activity(8);

            count.countDown();

            try {
                System.out.println(count+" t2");
                count.await();

            } catch (Exception e) {
                System.out.println("Eroare count down latch");
            }
            System.out.println("S-a terminat thread 2");
        }
    }
}
