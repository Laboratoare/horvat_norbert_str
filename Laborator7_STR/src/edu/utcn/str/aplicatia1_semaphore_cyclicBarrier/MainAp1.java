package edu.utcn.str.aplicatia1_semaphore_cyclicBarrier;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;



public class MainAp1 {


    public static void main(String[] args) {


        final Semaphore P9 = new Semaphore(1, true);
        final Semaphore P10 = new Semaphore(1, true);


        CyclicBarrier cyclicBarrier = new CyclicBarrier(2,
                new CBRunnable());


        Thread1 thread1 = new Thread1(P9, P10,cyclicBarrier );
        thread1.start();

        Thread2 thread2 = new Thread2(P9, P10,cyclicBarrier);
        thread2.start();


    }
    private static class CBRunnable implements Runnable {
        private static int iterationNum = 0;

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + ": iteration " + iterationNum++);
        }
    }

}












