package edu.utcn.str.aplicatia2;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

public class Thread3 extends Thread {
    Lock P10;
    CountDownLatch count;

    Thread3(Lock P10, CountDownLatch count) {
        this.count = count;
        this.P10 = P10;
    }

    public void run() {
        while(true) {

            Locatie P2 = new Locatie();
            P2.activity(2);
            Tranzitie T3 = new Tranzitie();
            P10.lock();
            T3.Tran(3);
            Locatie P3 = new Locatie();
            P3.activity(2, 5, 3);
            Tranzitie T5 = new Tranzitie();
            T5.Tran(5, 5);
            P10.unlock();
            Locatie P5 = new Locatie();
            P5.activity(5);

                 count.countDown();
            try {
                System.out.println(count + " t2");
                count.await();

            } catch (Exception e) {
                System.out.println("Eroare count down latch");
            }

        }

    }
}

